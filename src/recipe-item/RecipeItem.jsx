import React from 'react';
import styles from './RecipeItem.module.css'
import {useDispatch, useSelector} from "react-redux";
import {useAction} from "../hooks/useActions";
const RecipeItem = ({recipe}) => {
    const {favorites} = useSelector(state => state)

    console.log(favorites)

    const isExist = favorites.some(r => r.id === recipe.id)

    const {toggleFavorites} = useAction();

    return (
        <div className={styles.item}>
            <h3>{recipe.name}</h3>
            <button onClick={() => toggleFavorites(recipe)}>{isExist ? 'Remove from' : 'Add to favorites'}</button>
        </div>
    );
};

export default RecipeItem;