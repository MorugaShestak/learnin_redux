
import './App.css';
import RecipeItem from "./recipe-item/RecipeItem";

function App() {
  return (
    <div className="App">
        <RecipeItem recipe={{id: 1, name:'Лазанья'}}/>
        <RecipeItem recipe={{id: 2, name:'Каша'}}/>
        <RecipeItem recipe={{id: 3, name:'Хлопья'}}/>
    </div>
  );
}

export default App;
