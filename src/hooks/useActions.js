import {useDispatch} from "react-redux";
import {bindActionCreators} from "@reduxjs/toolkit";
import {actions} from "../store/favorites/favorite.slice";
import {useMemo} from "react";

const rootActions = {...actions}

export const useAction = () => {

    const dispatch = useDispatch();

    return useMemo(() => bindActionCreators(rootActions, dispatch), [dispatch])
}